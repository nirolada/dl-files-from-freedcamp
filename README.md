# Download Files from Freedcamp

An automation that logs into Freedcamp, enters a specified project
and downloads all of its' files with all of their versions.

Developed in Python 3.8.5

# Instructions

If you don't have python installed, get it from their website:
https://www.python.org/downloads/release/python-385/

Install selenium package (bash or cmd):
pip3 install selenium

Download the WebDriver binary supported by your browser and place it in the System PATH:
https://www.selenium.dev/documentation/en/webdriver/driver_requirements/

Update your personal browser name, login details and desired project on 'user.py'.
Run the code and tell me about bugs =]
