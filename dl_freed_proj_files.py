import time

from selenium.webdriver import Chrome, Firefox
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.ui import WebDriverWait

from constants import Freedcamp, Wait
from elements import Element
from user import My


def pick_browser():
    if My.BROWSER_NAME.lower() == 'firefox':
        return Firefox()
    elif My.BROWSER_NAME.lower() == 'chrome':
        return Chrome()
    else:
        raise Exception("Your browser is not supported.")


def wait(driver: Chrome, condition):
    return WebDriverWait(driver, Wait.TIMEOUT).until(condition)


def login(driver: Chrome):
    # enter login page
    driver.get(Freedcamp.LOGIN_URL)
    # type in username & password
    driver.find_element(By.NAME, Freedcamp.USERNAME).send_keys(My.USERNAME)
    driver.find_element(By.NAME, Freedcamp.PASSWORD).send_keys(My.PASSWORD)
    # click login button
    wait(driver, ec.element_to_be_clickable(Element.LOGIN_BTN)).click()


def enter_manage_sys(driver: Chrome):
    # wait for 'manage system' button and click it
    wait(driver, ec.element_to_be_clickable(Element.MANAGE_SYS_BTN)).click()


def click_project(driver: Chrome):
    # wait for desired project in the project bar and click it
    wait(driver, ec.element_to_be_clickable(Element.PROJECT_BTN)).click()


def focus_new_tab(driver: Chrome):
    # wait for driver to have 2 tabs
    wait(driver, lambda d: len(d.window_handles) == 2)
    # switch to the second tab
    driver.switch_to.window(driver.window_handles[1])


def open_project(driver: Chrome):
    # wait for desired project to show
    wait(driver, ec.presence_of_element_located(Element.PROJECT_TITLE))
    # wait for 'view' button and click it to open desired project page
    try:
        wait(driver, ec.element_to_be_clickable(Element.VIEW_BTN)).click()
    except:
        wait(driver, ec.element_to_be_clickable(Element.VIEW_BTN)).click()
    # focuse the new tab that opened up to control it
    focus_new_tab(driver)


def open_files_page(driver: Chrome):
    # wait for files tab and click it to open desired project's files page
    wait(driver, ec.element_to_be_clickable(Element.FILES_TAB)).click()


def get_file_elements(driver: Chrome):
    # get file elements on a page
    return wait(driver, ec.presence_of_all_elements_located(Element.FILE))


def get_dir_elements(driver: Chrome):
    # get directory elements on a page
    return wait(driver, ec.presence_of_all_elements_located(Element.DIRECTORY))


def get_version_elements(driver: Chrome):
    # if there's a 'Show all versions' button, click it
    try:
        wait(driver, ec.element_to_be_clickable(Element.SHOW_VERS_BTN)).click()
    finally:
        # finally, return all version elements
        return driver.find_element(*Element.VERSION)


def download_file_versions(driver: Chrome):
    versions = get_version_elements(driver)
    for v in versions:
        v.click()
        time.sleep(Wait.DL_INTERVAL)


def download_dir(driver: Chrome, dir_element):
    dir_element.click()
    wait(driver, ec.visibility_of_element_located(Element.PREV_DIR_ICON))
    download_all(driver)


def download_file(driver: Chrome, file_element):
    # open file versions page
    file_element.click()
    download_file_versions(driver)
    driver.back()


def download_all(driver: Chrome):
    # count files
    files_num = len(get_file_elements(driver))
    for i in range(files_num):
        # get file elements
        file_elements = get_file_elements(driver)
        # dl the file at the current index
        download_file(driver, file_elements[i])
    # count dirs
    dirs_num = len(get_dir_elements(driver))
    for j in range(dirs_num):
        # get dir elements
        dir_elements = get_dir_elements(driver)
        # dl the dir at the current index
        download_dir(driver, dir_elements[j])


if __name__ == '__main__':
    with pick_browser() as b:
        login(b)
        enter_manage_sys(b)
        click_project(b)
        open_project(b)
        open_files_page(b)
        download_all(b)
