from selenium.webdriver.common.by import By
from constants import Freedcamp


class Element:
    DIRECTORY = (By.XPATH, '//a[@class="folder_name"]')
    FILE = (By.XPATH, '//a[@class="filename break-word"]')
    FILES_TAB = (By.XPATH, '//a[@data-title="Files"]//parent::li')
    LOGIN_BTN = (By.CLASS_NAME, 'submitBtn')
    MANAGE_SYS_BTN = (By.LINK_TEXT, 'Manage System')
    PREV_DIR_ICON = (
        By.XPATH, '//*[name()="svg" and @class="svg-icon svg-icon-xs"]')
    PROJECT_BTN = (
        By.XPATH, f'//span[contains(text(),"{Freedcamp.PROJECT_NAME}")]//parent::div')
    PROJECT_TITLE = (
        By.XPATH, f'//h2[contains(text(),"{Freedcamp.PROJECT_NAME}")]')
    SHOW_VERS_BTN = (By.LINK_TEXT, 'Show all versions')
    VERSION = (By.XPATH, '//a[contains(text(),"Gurren")]')
    VIEW_BTN = (By.XPATH, '//span[contains(text(),"View")]//parent::a')
